package constants;

/**
 * Created by Miguel Pujadas Palenzuela
 */

public class GeometryViewConstants {
    public static final String START_DATE_BUTTON = "/html/body/div[1]/div[1]/div/div[2]/div/div[2]/div/div/div[1]/div/div/div[1]/div/div[1]/div/input";
    public static final String END_DATE_BUTTON = "/html/body/div[1]/div[1]/div/div[2]/div/div[2]/div/div/div[1]/div/div/div[1]/div/div[3]/div/input";

    public static final String START_MONTH_BUTTON = "/html/body/div[2]/div[1]/div/span";
    public static final String END_MONTH_BUTTON = "/html/body/div[3]/div[1]/div/span";

    public static final String MONTH_BEFORE_ARROW_BUTTON = "//span[@class='flatpickr-prev-month']";
    public static final String MONTH_AFTER_ARROW_BUTTON = "//span[@class='flatpickr-next-month']";
}
