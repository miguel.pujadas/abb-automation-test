package pages;
import tools.Tools;
import org.openqa.selenium.WebDriver;
import details.GeometryViewDetails;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Miguel Pujadas Palenzuela
 */
public class GeometryViewPage {

    private static List<String> months = Arrays.asList("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    static Tools tools = new Tools();
    public static GeometryViewDetails pageFactory;
    public GeometryViewPage(WebDriver driver){
        this.pageFactory = new GeometryViewDetails(driver);
    }

    public static void openStartDate(WebDriver driver){
        tools.waitForElementToBeDisplayed(driver, pageFactory.getStartDateButton(),10);
        pageFactory.getStartDateButton().click();
    }

    public static void openLastDate(WebDriver driver){
        tools.waitForElementToBeDisplayed(driver, pageFactory.getEndDateButton(),10);
        pageFactory.getEndDateButton().click();
    }

    public static String getStartMonth(WebDriver driver){

        tools.waitForElementToBeDisplayed(driver, pageFactory.getStartMonthButton(),10);
        return pageFactory.getStartMonthButton().getText();
    }

    public static String getEndMonth(WebDriver driver){
        tools.waitForElementToBeDisplayed(driver, pageFactory.getEndMonthButton(),10);
        return pageFactory.getEndMonthButton().getText();
    }

    public static void goMonthBefore(WebDriver driver){
        tools.waitForElementToBeDisplayed(driver, pageFactory.getMonthBeforeArrow(),10);
        pageFactory.getMonthBeforeArrow().click();
    }

    public static void goMonthAfter(WebDriver driver){
        tools.waitForElementToBeDisplayed(driver, pageFactory.getMonthAfterArrow(),10);
        pageFactory.getMonthAfterArrow().click();
    }

    public String[] getMonthAround(WebDriver driver){
        String currentMonth = getStartMonth(driver);
        int i = 0;
        String monthFlag = months.get(i);
        while (currentMonth!=monthFlag){
            i++;
        }
        String monthBefore = months.get(i-1);
        String monthAfter = months.get(i+1);
        String[] result = {monthBefore, monthAfter};
        return result;
    }
}
