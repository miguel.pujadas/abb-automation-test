package pages;
import tools.Tools;
import org.openqa.selenium.WebDriver;
import details.TopMenuDetails;

/**
 * Created by Miguel Pujadas Palenzuela
 */
public class TopMenuPage {
    public static TopMenuDetails pageFactory;
    public TopMenuPage(WebDriver driver){
        this.pageFactory = new TopMenuDetails(driver);
    }

    public static void goToGeometryView(WebDriver driver){
        Tools tools = new Tools();
        tools.waitForElementToBeDisplayed(driver, pageFactory.getGeometryViewButton(),10);
        pageFactory.getGeometryViewButton().click();
    }
}
