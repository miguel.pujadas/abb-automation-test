package details;
import constants.TopMenuConstants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
/**
 * Created by Miguel Pujadas Palenzuela
 */
public class TopMenuDetails extends TopMenuConstants {

    public TopMenuDetails(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
    public TopMenuDetails(){}

    @FindBy(xpath = TopMenuConstants.GEOMETRY_VIEW_BUTTON)
    private WebElement geometryViewButton;
    public WebElement getGeometryViewButton() {return geometryViewButton;}

}
