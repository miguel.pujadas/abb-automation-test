package details;
import constants.GeometryViewConstants;
import constants.TopMenuConstants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
/**
 * Created by Miguel Pujadas Palenzuela
 */
public class GeometryViewDetails extends GeometryViewConstants {

    public GeometryViewDetails(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
    public GeometryViewDetails(){}

    @FindBy(xpath = GeometryViewDetails.START_DATE_BUTTON)
    private WebElement startDateButton;
    public WebElement getStartDateButton() {return startDateButton;}

    @FindBy(xpath = GeometryViewDetails.END_DATE_BUTTON)
    private WebElement endDateButton;
    public WebElement getEndDateButton() {return endDateButton;}

    @FindBy(xpath = GeometryViewDetails.START_MONTH_BUTTON)
    private WebElement startMonthButton;
    public WebElement getStartMonthButton() {return startMonthButton;}

    @FindBy(xpath = GeometryViewDetails.END_MONTH_BUTTON)
    private WebElement endMonthButton;
    public WebElement getEndMonthButton() {return endMonthButton;}

    @FindBy(xpath = GeometryViewDetails.MONTH_BEFORE_ARROW_BUTTON)
    private WebElement monthBeforeArrow;
    public WebElement getMonthBeforeArrow() {return monthBeforeArrow;}

    @FindBy(xpath = GeometryViewDetails.MONTH_AFTER_ARROW_BUTTON)
    private WebElement monthAfterArrow;
    public WebElement getMonthAfterArrow() {return monthAfterArrow;}
}
