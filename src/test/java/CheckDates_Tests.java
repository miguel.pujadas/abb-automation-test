/**
 * Created by Miguel Pujadas Palenzuela
 */
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import scenarios.CheckDates_Scenario;

import java.net.MalformedURLException;

public class CheckDates_Tests extends CheckDates_Scenario {
    private static WebDriver driver;
    @BeforeMethod(alwaysRun = true)
    public void setup() throws MalformedURLException {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        DesiredCapabilities capability = DesiredCapabilities.chrome();
        driver = new ChromeDriver(capability);
        driver.manage().window().maximize();
        driver.get("http://qrt-assignment.northeurope.cloudapp.azure.com");
    }
    @Test (groups = { "validate_quantities" })
    public void checkValues() throws Exception {
        checkCorrectMonthRange(driver);
    }

    @AfterMethod(alwaysRun = true)
    public void teardown() {
        driver.quit();
    }
}
