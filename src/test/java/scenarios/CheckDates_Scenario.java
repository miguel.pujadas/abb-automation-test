package scenarios;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pages.GeometryViewPage;
import pages.TopMenuPage;

/**
 * Created by Miguel Pujadas Palenzuela
 */
public class CheckDates_Scenario {
    public void checkCorrectMonthRange(WebDriver driver) throws InterruptedException {
        TopMenuPage menuPage = new TopMenuPage(driver);
        GeometryViewPage geometryViewPage = new GeometryViewPage(driver);
        menuPage.goToGeometryView(driver);
        geometryViewPage.openStartDate(driver);
        String currentMonth = geometryViewPage.getStartMonth(driver);
        geometryViewPage.goMonthBefore(driver);
        Assert.assertEquals(currentMonth, geometryViewPage.getMonthAround(driver)[0]);
        geometryViewPage.goMonthAfter(driver);
        geometryViewPage.goMonthAfter(driver);
        Assert.assertEquals(currentMonth, geometryViewPage.getMonthAround(driver)[1]);
    }
}
