**----REQUIREMENTS----**

* Google Chrome
* Maven


**----HOW TO USE----**

1. Clone the project
2. Navigate through console to the root of the project
3. Type: mvn test

**----TO DO----**

1. Check changing months
2. Check when starting date is later than end date
3. Parameterize input data to select dates
4. Parameterize input data to select what tests to run
5. Integration testing with API
6. Load testing